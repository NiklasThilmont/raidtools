/**
 * Created by Niklas on 06.02.2016.
 */

//! @brief Constructs a complete Raid Setup from the given JSON string
//!        Some of the functions are closely tied to the DOM structure it
//!        generates via "toTable".
//! @param  inputJSON JSON Input String
//! @param  updateFunctionName  Name of the function that is used to communicate with the DOM Structure
//!                             Function name is without ().
function RaidSetup(inputJSON, updateFunctionName, updateRoleFunctionName, removeBossFunction)
{
    // Raid data. JSON object, array of bosses which contains information
    this._rawData = inputJSON;

    // Raid Setup data
    this._data = null;

    // associative array that maps a "uID" identifier into
    // 2 integer indizees representing a specific player in the complete setup
    this._mappings = null;

    this.upFunc = updateFunctionName;

    this.upRoleFunc = updateRoleFunctionName;

    this.rmBossFuncName = removeBossFunction;


    // Prefix used for unqiue IDs for the player name input fields. Is combined with
    // the current position ID to form an unique selector of the current name input field.
    this._inputFieldPrefix = "RaidPlanInpt";

    this._classFieldPrefix = "RaidPlanClass";


    this.mapping = function(indexBoss, indexPlayer)
    {
        this.playerIndex = indexPlayer;
        this.bossIndex = indexBoss;
    }

    this.createMapping = function()
    {
        this._mappings = new Array();
        for(var i = 0; i < this._data.RaidTeam.length; i++)
        {
            var bData = this._data.RaidTeam[i];
            for(var j = 0; j < bData.teamSetup.classes.length; j++)
            {
                this._mappings[bData.teamSetup.classes[j].uID] = new this.mapping(i,j);
            }
        }
        console.log(this._mappings);
    }

    this._addSpecialRoles = function(playerData)
    {
        var res = '';
        for(var i = 0; i < playerData.specialRoles.length; i++)
        {

            res += playerData.specialRoles[i];
            if(i < playerData.specialRoles.length - 1)
                res += ", ";
        }
        return res;
    }

    //! @brief  creates a single table based on the given JSON object bossData
    //! @param  bossData    The data for the boss. Structure:
    //!                     id : id for table
    //!                     name : bossname
    //1                     teamSetup : array of object with members (pName, pRole, uID)
    //!                     size : size of the party
    //! @return HTML String rep of the whole table
    this._oneTable = function(bossData)
    {

        var res = '';
        res = res + "<div class='col-md-4'>";
        res = res + "<table id='" + bossData.id + "' class='table table-condensed table-bordered table-striped'>";

        // Table Caption.
        var captionName =  "<i class='input-group-addon'>" + bossData.name + "</i>";
        var removeBossFunction = "onclick='" + this.rmBossFuncName + "(&quot;"+  bossData.id + "&quot;);'";
        var captionDelete = "<a class='input-group-addon'" + removeBossFunction + "><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></a>";
        res = res + "<caption>" + captionName + captionDelete + "</caption>";

        // Table headers.
        res = res + "<tr><th>Role</th><th>Name</th><th>Team</th><th>Special</th></tr>";
        for(var i = 0; i < bossData.teamSetup.classes.length; i++)
        {
            // Table row. Table rows contain an ID and some table data.
            res = res + "<tr id='" + bossData.teamSetup.classes[i].uID + "'>\n";

            // Data Set 1: The Role.
            var roleId = this._classFieldPrefix + bossData.teamSetup.classes[i].uID;
            var role = bossData.teamSetup.classes[i].pRole;
            var keyUpRoleFunc = this.upRoleFunc + "(&quot;" + bossData.teamSetup.classes[i].uID + "&quot;)";
            var inputRoleName = "<input id='" + roleId + "' value='" + role + "' onkeyup='" + keyUpRoleFunc + "'>";

            res = res + "<td>" + inputRoleName + "</td>";

            // Data Set 2: Input field with name.
            var nameID = this._inputFieldPrefix + bossData.teamSetup.classes[i].uID;
            var name = bossData.teamSetup.classes[i].pName;
            var keyUpFunc = this.upFunc + "(" + "&quot;" + bossData.teamSetup.classes[i].uID + "&quot;)";
            var inputField = "<input id='" + nameID + "' value='" + name + "' onkeyup='" + keyUpFunc + "'>";
            res = res + "<td>" + inputField + "</td>";

            // Data Set 3.
            if(bossData.teamSetup.classes[i].pTeam != null) {
                res = res + "<td>" + bossData.teamSetup.classes[i].pTeam + "</td>";
            }
            else {
                res = res + "<td>-</td>";
            }
            if(bossData.teamSetup.classes[i].specialRoles != null) {
                res = res + "<td>" + this._addSpecialRoles(bossData.teamSetup.classes[i]) + "</td>";
            } else {
                res = res + "<td>-</td>";
            }

            res = res + "</tr>\n"
        }
        res += "</table>";
        res += "</div>";
        return res;
    };

    //! @brief processes the given raw json data into a raid setup
    this.create = function() {
        try {
            this._data = JSON.parse(this._rawData);

        }
        catch(e)
        {
            alert("Error while parsing data. Exception is logged to console.")
            console.log(e);
        }
        this.createMapping();
    };

    //! @brief  creates a table representation of the raid setup.
    //!         for each td element, the unqize party member IDs are being used
    //!         for each boss subtable, the uniqze boss ID is being used
    //! @return tables for each boss.
    this.toTable = function() {
        var tables = '';
        for(var i = 0; i < this._data.RaidTeam.length; i++)
        {
            tables = tables + this._oneTable(this._data.RaidTeam[i]);
        }
        return tables;

    };

    //! @brief  Updates the name that matches the given uID in the setup.
    //!         The user ID is composed of the bossID and a number from 0 to n.
    //!         This user ID must be uniqe through the whole boss. This is implicitly
    //!         the case when the Boss-IDs are unique.
    //! @param  uID     The unique user ID, unique through all Bosses.
    //! @param  name    The new name.
    this.updateName = function(uID, name)
    {
        var ind = this._mappings[uID];
        this._data.RaidTeam[ind.bossIndex].teamSetup.classes[ind.playerIndex].pName = name;
    };

    //! @brief  Updates the role that matches the given uID in the setup.
    //!         The user ID is composed of the bossID and a number from 0 to n.
    //!         This user ID must be uniqe through the whole boss. This is implicitly
    //!         the case when the Boss-IDs are unique.
    //! @param  uID     The unique user ID, unique through all Bosses.
    //! @param  role    The new role.
    this.updateClass = function(uID, role)
    {
        var ind = this._mappings[uID];
        this._data.RaidTeam[ind.bossIndex].teamSetup.classes[ind.playerIndex].pRole = role;
    };

    this.removeBoss = function(bossID)
    {
        console.log(bossID);
        for(var i = 0; i < this._data.RaidTeam.length; i++)
        {
            if(this._data.RaidTeam[i].id == bossID)
            {
                this._data.RaidTeam.splice(i, 1);
            }
        }
        console.log(this._data);
    };

    //! @brief  returns a string rep of the given boss team
    this.bossTeamToString = function(bossData)
    {
        var res = "";
        for(var i = 0; i < bossData.teamSetup.classes.length; i++)
        {
            res += bossData.teamSetup.classes[i].pName;
            res += " - ";
            res += bossData.teamSetup.classes[i].pRole;
            if(i < bossData.teamSetup.classes.length - 1 )
            {

                res += " | ";
            }
        }
        return res;
    };

    //! @brief  Returns a string rep of the raid composition
    //! @return string rep with pattern:
    //!         Bossname: player - role | player - role | ... | player - role \n
    //!         Bossname: ...etc...
    this.toString = function()
    {

        var res = '';
        for(var i = 0; i < this._data.RaidTeam.length; i++)
        {
            res += this._data.RaidTeam[i].name + ': ';
            res += this.bossTeamToString(this._data.RaidTeam[i]);
            res += "\n\n\n";
        }
        return res;
    };

    //! @brief  Returns a players class in the given setup
    //! @param  bossData    Setup data for one boss/group comp
    //! @param  pName       Player name
    this.getPlayerClass = function(bossData, pName)
    {
        var res;
        for(var i = 0; i < bossData.teamSetup.classes.length; i++)
        {
            var n0 = bossData.teamSetup.classes[i].pName.toUpperCase();
            var n1 = pName.toUpperCase();
            if(n0 == n1)
            {
                res = bossData.teamSetup.classes[i].pRole;
            }
        }
        return res;
    };

    //! @brief  Returns player name at index pIndex
    this.getPlayerName = function(bossData, pIndex)
    {
        return bossData.teamSetup.classes[pIndex].pName;
    };


    //! @brief  Returns the raid team representation, sorted by players
    //! @return String rep in this form:
    //!         <pname> : <class> <class> <class> |
    this.toStringPlayerSorted = function()
    {
        var i = 0;
        var j = 0;
        var res ='';
        for(i = 0; i < this._data.RaidTeam[0].teamSetup.classes.length; i++)
        {
            var name = this.getPlayerName(this._data.RaidTeam[0], i);
            res += name;
            res += " : ";
            for(j = 0; j < this._data.RaidTeam.length; j++)
            {
                res += "<";
                res += this.getPlayerClass(this._data.RaidTeam[j], name);
                res += "> ";
            }
            res += "\n";
        }
        return res;
    };


    this._hasSpecialRole = function(pData)
    {
        return pData.specialRoles != null && pData.specialRoles.length >= 1;
    }

    //! @brief  Prints the Setup, sorted by special Roles per boss
    this.toStringSpecialBased = function()
    {
        var res = '';
        for(var i = 0; i < this._data.RaidTeam.length; i++)
        {
            res += this._data.RaidTeam[i].name + ": ";
            var classes = this._data.RaidTeam[i].teamSetup.classes;
            for(var j = 0; j < classes.length; j++)
            {

                if(this._hasSpecialRole(classes[j]))
                {
                    res += classes[j].pName + "->";
                    for(var k = 0; k < classes[j].specialRoles.length; k++)
                    {
                        res += classes[j].specialRoles[k];
                        if(k < classes[j].specialRoles.length - 1)
                            res += ", ";
                    }
                    res += " | "
                }

            }
            res += "\n";
            res += "\n";

        }
        return res;
    };

    //! @brief  Returns stringified JSON of setup, indented with 4 whitespace
    this.toJSONString = function()
    {
        return JSON.stringify(this._data, null, 4);
    };

    this.toJSON = function()
    {
        return JSON.stringify(this._data);
    };

    //! @brief  Sorts the teams with the given comparator function.
    this.sortTeams = function(comparator)
    {
        for(var i = 0; i < this._data.RaidTeam.length; i++)
        {
            this._data.RaidTeam[i].teamSetup.classes.sort(comparator);
        }
    }

    //! @brief  Returns a comparator function that sorts a specific team
    //!         By it's members team numbers.
    //!         Has to be called on .classes member.
    //! @param  doDescSort  true if sorting is supposed to be descending, otheriwse false.
    this.getTeamSorter = function(doDescSort)
    {
        if(doDescSort)
        {
            return function(a,b) {
                return b.pTeam - a.pTeam;
            }
        }
        else
        {
            return function(a,b) {
                return a.pTeam - b.pTeam;
            }
        }
    }


    //! @brief  Checks if the player name with the given ID is unique in the current
    //!         boss group.
    //! @param  uniqueSlotID    unique player ID
    //! @return true if name is unqiue in boss group
    this.isUniqueName = function(uniqueSlotID)
    {
        var indices = this._mappings[uniqueSlotID];
        var bData = this._data.RaidTeam[indices.bossIndex].teamSetup.classes;
        var thisName = bData[indices.playerIndex].pName;
        var isUnique = true;
        for(var i = 0; i < bData.length; i++)
        {
            if(i != indices.playerIndex)
            {
                isUnique = isUnique && (bData[i].pName != thisName);
            }
        }
        return isUnique;
    }

    //! @brief  Returns an array of all uIDs
    this.getIDArray = function() {
        var res = new Array();
        for(var i = 0; i < this._data.RaidTeam.length; i++)
        {
            var bData = this._data.RaidTeam[i].teamSetup.classes;
            for(var j = 0; j < bData.length; j++)
            {
                res[res.length] = bData[j].uID;
            }
        }
        return res;

    }
}